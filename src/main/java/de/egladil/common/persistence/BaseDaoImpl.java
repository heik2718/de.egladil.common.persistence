//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.persistence;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Provider;
import com.google.inject.persist.Transactional;

/**
 * BaseDaoImpl
 */
public abstract class BaseDaoImpl<T extends IDomainObject> implements IBaseDao<T> {

	private static final Logger LOG = LoggerFactory.getLogger(BaseDaoImpl.class);

	private Provider<EntityManager> emp;

	/**
	 * Erzeugt eine Instanz von BaseDaoImpl
	 */
	public BaseDaoImpl(final Provider<EntityManager> emp) {
		this.emp = emp;
	}

	@Override
	public List<T> findAll(final Class<T> clazz) {
		final String statement = "SELECT e FROM " + clazz.getSimpleName() + " e";
		final List<T> resultList = emp.get().createQuery(statement.toString(), clazz).getResultList();
		return resultList;
	}

	@Override
	public Optional<T> findById(final Class<T> clazz, final long id) {
		final T entity = emp.get().find(clazz, id);
		return entity == null ? Optional.empty() : Optional.of(entity);
	}

	@Override
	@Transactional
	public T persist(final T entity) throws PersistenceException {
		T result = null;
		final EntityManager em = emp.get();
		if (entity.getId() != null) {
			result = em.merge(entity);
		} else {
			em.persist(entity);
			result = entity;
		}
		return result;
	}

	@Override
	@Transactional
	public List<T> persist(final List<T> entities) throws PersistenceException {
		if (entities == null) {
			throw new IllegalArgumentException("entities null");
		}
		if (entities.isEmpty()) {
			return Collections.emptyList();
		}
		final List<T> result = new ArrayList<>();
		final EntityManager em = emp.get();
		for (final T entity : entities) {
			if (entity.getId() != null) {
				result.add(em.merge(entity));
			} else {
				em.persist(entity);
				result.add(entity);
			}
		}
		return result;
	}

	@Override
	@Transactional
	public String delete(final T entity) throws PersistenceException {
		final EntityManager em = emp.get();
		final T zuLoeschen = em.merge(entity);
		em.remove(zuLoeschen);
		return "";
	}

	@Override
	@Transactional
	public void delete(final List<T> entities) throws PersistenceException {
		if (entities == null) {
			throw new IllegalArgumentException("entities null");
		}
		if (!entities.isEmpty()) {
			final EntityManager em = emp.get();
			for (final T entity : entities) {
				em.remove(entity);
			}
		}
	}

	@Override
	public Optional<T> findByUniqueKey(final Class<T> clazz, final String attributeName, final String attributeValue)
		throws EgladilStorageException {
		if (clazz == null) {
			throw new IllegalArgumentException("clazz null");
		}
		if (attributeName == null) {
			throw new IllegalArgumentException("attributeGetterName null");
		}
		if (StringUtils.isBlank(attributeValue)) {
			throw new IllegalArgumentException("attributeValue blank");
		}
		try {
			clazz.getDeclaredField(attributeName);
			final String stmt = "SELECT e from " + clazz.getSimpleName() + " e where e." + attributeName + " = :value";
			LOG.debug(stmt);

			final EntityManager entityManager = getEntityManager();
			final TypedQuery<T> query = entityManager.createQuery(stmt, clazz);
			query.setParameter("value", attributeValue);

			final List<T> treffer = query.getResultList();
			LOG.debug("Anzahl Treffer={}", treffer.size());
			if (treffer.size() == 1) {
				return Optional.of(treffer.get(0));
			}
			if (treffer.isEmpty()) {
				return Optional.empty();
			}
			throw new EgladilStorageException(
				"Datenfehler: mehr als eine Entity mit [attributName=" + attributeName + ", attributWert=" + attributeValue + "]");
		} catch (NoSuchFieldException | SecurityException e) {
			throw new IllegalArgumentException(
				"Class " + clazz.getSimpleName() + " hat kein lesbares Attribut [" + attributeName + "]: " + e.getMessage(), e);
		} catch (final IllegalArgumentException e) {
			throw e;
		} catch (final Exception e) {
			throw new EgladilStorageException("Unerwarteter Fehler beim Suchen der Entity " + clazz.getSimpleName() + " mit ["
				+ attributeName + ", attributWert=" + attributeValue + "]: " + e.getMessage(), e);
		}
	}

	protected EntityManager getEntityManager() {
		return emp.get();
	}
}
