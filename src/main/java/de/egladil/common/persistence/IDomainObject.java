//=====================================================
// Projekt: de.egladil.bv.aas
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.persistence;

import java.io.Serializable;

/**
 * IDomainObject
 */
public interface IDomainObject extends Serializable {

	Long getId();
}
