//=====================================================
// Projekt: de.egladil.exceptions
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.persistence;

/**
 * @author heike
 *
 */
public class EgladilDuplicateEntryException extends RuntimeException {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private String uniqueIndexName;

	/**
	 * @param message
	 */
	public EgladilDuplicateEntryException(final String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public EgladilDuplicateEntryException(final Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public EgladilDuplicateEntryException(final String message, final Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public EgladilDuplicateEntryException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public String getUniqueIndexName() {
		return uniqueIndexName;
	}

	public void setUniqueIndexName(final String uniqueIndexName) {
		this.uniqueIndexName = uniqueIndexName;
	}
}
