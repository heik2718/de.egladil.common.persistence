//=====================================================
// Projekt: de.egladil.common.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.persistence;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.persist.UnitOfWork;

/**
 * EgladilPersistFilter fork von com.google.inject.persist.PersistFilter avoiding to start persisService in init.
 */
public class EgladilPersistFilter implements Filter {

	private static final Logger LOG = LoggerFactory.getLogger(EgladilPersistFilter.class);

	private final UnitOfWork unitOfWork;

	private boolean started = false;

	/**
	 * EgladilPersistFilter
	 */
	@Inject
	public EgladilPersistFilter(final UnitOfWork unitOfWork) {
		this.unitOfWork = unitOfWork;
	}

	@Override
	public void init(final FilterConfig filterConfig) throws ServletException {
		// Hier nicht den persistService starten, weil das bereits durch das JpaPersistPrivateModule getan wurde.
		LOG.info("Filter initialized");
	}

	@Override
	public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain filterChain)
		throws IOException, ServletException {
		if (!started) {
			unitOfWork.begin();
			started = true;
		}
		try {
			filterChain.doFilter(request, response);
		} finally {
			unitOfWork.end();
			started = false;
		}
	}

	@Override
	public void destroy() {
		unitOfWork.end();
		LOG.info("Filter stopped");
	}
}
