//=====================================================
// Projekt: de.egladil.exceptions
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.persistence;

/**
 * EgladilConcurrentModificationException
 */
public class EgladilConcurrentModificationException extends RuntimeException {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	/**
	 * Erzeugt eine Instanz von EgladilConcurrentModificationException
	 */
	public EgladilConcurrentModificationException(final String message) {
		super(message);
	}

	/**
	 * Erzeugt eine Instanz von EgladilConcurrentModificationException
	 */
	public EgladilConcurrentModificationException(final Throwable cause) {
		super(cause);
	}

	/**
	 * Erzeugt eine Instanz von EgladilConcurrentModificationException
	 */
	public EgladilConcurrentModificationException(final String message, final Throwable cause) {
		super(message, cause);
	}

	/**
	 * Erzeugt eine Instanz von EgladilConcurrentModificationException
	 */
	public EgladilConcurrentModificationException(final String message, final Throwable cause, final boolean enableSuppression,
		final boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
