package de.egladil.common.persistence;

import javax.persistence.EntityManager;

@Deprecated
public interface IEntityManagerProducer {

	/**
	 *
	 * @return
	 */
	EntityManager getEntityManager();

	/**
	 *
	 * @param em
	 */
	void dispose(EntityManager em);

	/**
	 *
	 */
	void shutDown();

}