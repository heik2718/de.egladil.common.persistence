//=====================================================
// Projekt: de.egladil.bv.gottapp
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.persistence.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.google.common.base.Splitter;

public final class PrettyStringUtils {

	/**
	 * Das Default-Ergebnis dieser Operation ist ein Komma-Blank-separierter String aus den Elementen der Collection,
	 * sofern diese mehr als ein Element enthält.
	 *
	 * @param strings
	 * @return String nie null.
	 */
	public static String collectionToDefaultString(final Collection<String> strings) {
		return collectionToString(strings, ", ");
	}

	/**
	 * Wandelt eine Collection von Strings in einen durch separationChars getrennten Einzelstring ohne lLeerzeichen um.<br>
	 * <br>
	 * Wenn strings null oder leer ist, wird "" zurückgegeben.<br>
	 * Wenn strings nur ein Element enthält, wird dieses zurückgegeben.<br>
	 * Wenn separationChars null ist oder Länge 0 hat, werden alle Elemente aneinandergeklatscht.
	 *
	 * @param strings Collection.
	 * @param separationChars
	 * @return String nie null.
	 */
	public static String collectionToString(final Collection<String> strings, final String separationChars) {
		if ((strings == null) || (strings.isEmpty())) {
			return "";
		}
		final List<String> copy = new ArrayList<>();
		final Iterator<String> iterator = strings.iterator();
		while(iterator.hasNext()){
			copy.add(iterator.next());
		}
		return listToString(copy, separationChars);
	}

	private static String listToString(final List<String> strings, final String separationChars){
		final StringBuffer sb = new StringBuffer();
		final String separator = (separationChars == null || separationChars.isEmpty()) ? "" : separationChars;

		for (int i = 0; i < strings.size() - 1; i++) {
			sb.append(strings.get(i));
			sb.append(separator);
		}
		sb.append(strings.get(strings.size() - 1));
		return sb.toString();
	}

	/**
	 * Überschreibt den String bitweise mit Nullen.
	 *
	 * @param secret
	 */
	public static void erase(String secret) {
		if (secret != null) {
			erase(secret.toCharArray());
			secret = null;
		}
	}

	/**
	 * Setzt alle Elementa gleich 0.
	 *
	 * @param secret
	 */
	public static void erase(final char[] secret) {
		if (secret != null) {
			for (int i = 0; i < secret.length; i++) {
				secret[i] = 0x00;
			}
		}
	}

	/**
	 * Nimmt einen durch separationChars getrennten String, entfernt alle Teile, die mehrfach vorkommen und gibt den
	 * resultierenden String mit dem gleichen separationChar getrennt als String zurück.
	 *
	 * @param string String falls null, dann ""
	 * @param separationChars
	 * @return String nie null
	 */
	public static String toStringOhneDubletten(final String string, final String separationChars) {
		if (StringUtils.isBlank(string)) {
			return "";
		}
		final Set<String> token = new HashSet<>();

		final List<String> alle = Splitter.on(separationChars).trimResults().splitToList(string);
		for (final String t : alle) {
			token.add(t);
		}
		return collectionToString(token, separationChars);
	}
}
