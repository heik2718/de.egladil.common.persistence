//=====================================================
// Projekt: de.egladil.common.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.persistence.protokoll.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.common.config.IEgladilConfiguration;
import de.egladil.common.persistence.protokoll.Ereignis;
import de.egladil.common.persistence.protokoll.IEreignisDao;
import de.egladil.common.persistence.protokoll.IProtokollService;

/**
 * ProtokollService
 */
@Singleton
public class ProtokollService implements IProtokollService {

	private static final Logger LOG = LoggerFactory.getLogger(ProtokollService.class);

	private final IEreignisDao ereignisDao;

	private final boolean protokolliereInDB;

	/**
	 * Erzeugt eine Instanz von ProtokollService
	 */
	@Inject
	public ProtokollService(final IEreignisDao ereignisDao, final IEgladilConfiguration configuration) {
		this.ereignisDao = ereignisDao;
		final String protokollmodus = configuration.getProperty("protokoll.modus");
		protokolliereInDB = "DB".equals(protokollmodus);
	}

	@Override
//	@Transactional
	public Ereignis protokollieren(final String kuerzelEreignisart, final String wer, final String was) {
		if (StringUtils.isAnyBlank(new String[] { kuerzelEreignisart, wer, was })) {
			LOG.error("eins von kuerzelEreignisart, wer, was ist blank");
			return null;
		}

		final Ereignis ereignis = new Ereignis(kuerzelEreignisart, wer, was);
		if (!protokolliereInDB) {
			protokolliereInsLog(ereignis);
		} else {
			try {
				return ereignisDao.persist(ereignis);
			} catch (final Exception e) {
				LOG.error(e.getMessage(), e);
				protokolliereInsLog(ereignis);
			}
		}
		return ereignis;
	}

	@Override
	public Ereignis protokollieren(final Ereignis ereignis) {
		if (ereignis == null) {
			throw new IllegalArgumentException("ereignis darf nicht null sein");
		}
		if (!protokolliereInDB) {
			protokolliereInsLog(ereignis);
		} else {
			try {
				return ereignisDao.persist(ereignis);
			} catch (final Exception e) {
				LOG.error(e.getMessage(), e);
				protokolliereInsLog(ereignis);
			}
		}
		return ereignis;
	}

	private void protokolliereInsLog(final Ereignis ereignis) {
		LOG.warn("Alternative Protokollierung: {}", ereignis);
	}

	@Override
	public List<Ereignis> findByEreignisart(final String kuerzelEreignisart) {
		if (StringUtils.isBlank(kuerzelEreignisart)) {
			throw new IllegalArgumentException("kuerzelEreignisart blank");
		}
		return ereignisDao.findByEreignisart(kuerzelEreignisart);
	}
}
