//=====================================================
// Projekt: de.egladil.common.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.persistence.protokoll;

import java.util.List;

import de.egladil.common.persistence.IBaseDao;

/**
 * IEreignisDao
 */
public interface IEreignisDao extends IBaseDao<Ereignis> {

	/**
	 * Sucht alle Ereignisse mit der gegebenen Ereignisart.
	 *
	 * @param kuerzelEreignisart String
	 * @return List
	 */
	List<Ereignis> findByEreignisart(String kuerzelEreignisart);

}
