//=====================================================
// Projekt: de.egladil.common.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.persistence.protokoll;

import java.util.List;

/**
 * IProtokollService
 */
public interface IProtokollService {

	/**
	 * Erzeugt einen Protokolleintrag. <br>
	 * <br>
	 * <strong>Alle Exceptions werden gefangen und geloggt.</strong>
	 *
	 * @param kuerzelEreignisart String
	 * @param wer String
	 * @param was String
	 */
	Ereignis protokollieren(String kuerzelEreignisart, String wer, String was);

	/**
	 * Erzeugt einen Protokolleintrag. <br>
	 * <br>
	 * <strong>Alle Exceptions werden gefangen und geloggt.</strong>
	 *
	 * @param ereignis
	 * @return Ereignis
	 */
	Ereignis protokollieren(Ereignis ereignis);

	/**
	 * Sucht alle Ereignisse mit dem gegebenen Ereignisart-Kürzel.
	 *
	 * @param kuerzelEreignisart
	 * @return List
	 */
	List<Ereignis> findByEreignisart(String kuerzelEreignisart);

}
