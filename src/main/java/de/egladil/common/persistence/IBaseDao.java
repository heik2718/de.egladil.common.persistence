//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.persistence;

import java.util.List;
import java.util.Optional;

import javax.persistence.PersistenceException;

/**
 * IBaseDao
 */
public interface IBaseDao<T extends IDomainObject> {

	/**
	 *
	 * @return
	 */
	List<T> findAll(Class<T> clazz) throws PersistenceException;

	/**
	 *
	 * @param id
	 * @return
	 */
	Optional<T> findById(Class<T> clazz, long id);

	/**
	 * C oder U.<br>
	 * <br>
	 * <b>Achtung:</b> aus Gründen, die ich noch nicht verstehe, kann man PersistenceExceptions nicht fangen und
	 * umwandeln. Das ist so lange dem Aufrufer vorbehalten.
	 *
	 * @param entity
	 * @return
	 */
	T persist(T entity) throws PersistenceException;

	/**
	 * C oder U.<br>
	 * <br>
	 * <b>Achtung:</b> aus Gründen, die ich noch nicht verstehe, kann man PersistenceExceptions nicht fangen und
	 * umwandeln. Das ist so lange dem Aufrufer vorbehalten.
	 *
	 * @param entities
	 * @return
	 */
	List<T> persist(List<T> entities) throws PersistenceException;

	/**
	 * Delete. Nicht void wegen Mockito.<br>
	 * <br>
	 * <b>Achtung:</b> aus Gründen, die ich noch nicht verstehe, kann man PersistenceExceptions nicht fangen und
	 * umwandeln. Das ist so lange dem Aufrufer vorbehalten.
	 *
	 * @param entity
	 */
	String delete(T entity) throws PersistenceException;

	/**
	 * D.<br>
	 * <br>
	 * <b>Achtung:</b> aus Gründen, die ich noch nicht verstehe, kann man PersistenceExceptions nicht fangen und
	 * umwandeln. Das ist so lange dem Aufrufer vorbehalten.
	 *
	 * @param entities
	 */
	void delete(List<T> entities) throws PersistenceException;

	/**
	 * Sucht die Entity, deren Attribut attributeName den exakten Wert attributeValue hat.
	 *
	 * @param clazz
	 * @param attributeName
	 * @param attributeValue
	 * @return T oder null
	 * @throws EgladilServerException
	 * @throws EgladilStorageException
	 */
	Optional<T> findByUniqueKey(Class<T> clazz, String attributeName, String attributeValue)
		throws EgladilStorageException;

}
