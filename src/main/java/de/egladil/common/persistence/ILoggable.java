//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.persistence;

import java.io.Serializable;

/**
 * ILoggable
 */
public interface ILoggable extends Serializable {

	/**
	 * Beim Loggen eines vermutlichen BOTs werden auch die geheimen Attribute wie z.B. Passwörter mitgeloggt.
	 *
	 * @return
	 */
	String toBotLog();
}
