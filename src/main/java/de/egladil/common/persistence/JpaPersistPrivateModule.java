//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.persistence;

import java.lang.annotation.Annotation;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import com.google.inject.Inject;
import com.google.inject.PrivateModule;
import com.google.inject.Singleton;
import com.google.inject.persist.PersistService;
import com.google.inject.persist.UnitOfWork;
import com.google.inject.persist.jpa.JpaPersistModule;

import de.egladil.common.config.IEgladilConfiguration;

/**
 * JpaPersistPrivateModule
 */
public abstract class JpaPersistPrivateModule extends PrivateModule {

	private final Class<? extends Annotation> qualifier;

	private final String pathConfigRoot;

	@Singleton
	public static class JPAInitializer {

		@Inject
		public JPAInitializer(final PersistService service) {
			service.start();
		}
	}

	public JpaPersistPrivateModule(final Class<? extends Annotation> qualifier, final String pathConfigRoot) {
		this.qualifier = qualifier;
		this.pathConfigRoot = pathConfigRoot;
	}

	@Override
	protected void configure() {
		final String persistenceUnitName = getPersistenceUnitName();
		final Map<String, String> configurationMap = getConfigurations().getConfigurationMap();
		install(new JpaPersistModule(persistenceUnitName).properties(configurationMap));
		bind(JPAInitializer.class).asEagerSingleton();

		rebind(qualifier, EntityManagerFactory.class, EntityManager.class, PersistService.class, UnitOfWork.class, EgladilPersistFilter.class);

		doConfigure();
	}

	private void rebind(final Class<? extends Annotation> qualifier, final Class<?>... classes) {
        for (final Class<?> clazz : classes) {
            rebind(qualifier, clazz);
        }
    }

    private <T> void rebind(final Class<? extends Annotation> qualifier, final Class<T> clazz) {
        bind(clazz).annotatedWith(qualifier).toProvider(binder().getProvider(clazz));
        expose(clazz).annotatedWith(qualifier);
}

	protected abstract String getPersistenceUnitName();

	protected abstract IEgladilConfiguration getConfigurations();

	protected abstract void doConfigure();

	protected String getPathConfigRoot() {
		return pathConfigRoot;
	}
}
