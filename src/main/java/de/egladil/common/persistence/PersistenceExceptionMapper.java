//=====================================================
// Projekt: de.egladil.mkm.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.common.persistence;

import java.util.Optional;

import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceException;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.hibernate.exception.ConstraintViolationException;

/**
 * PersistenceExceptionMapper inspiziert PersistenceException, um darin spezifische gewrappete Exceptions zu finden und
 * leichter handhabbar zu machen.
 */
public final class PersistenceExceptionMapper {

	/**
	 * Schaut, ob es eine SQLIntegrityConstraintViolationException oder ConstraintViolationExceptiongibt und wandelt sie
	 * um in eine EgladilDuplicateEntryException, falls vorhanden. Der Name des verletzten Constraints wird in die
	 * EgladilDuplicateEntryException gesetzt.
	 *
	 * @param Exception
	 * @return Optional von EgladilDuplicateEntryException
	 */
	public static Optional<EgladilDuplicateEntryException> toEgladilDuplicateEntryException(Exception e) {
		if (e instanceof PersistenceException) {
			int index = ExceptionUtils.indexOfThrowable(e, ConstraintViolationException.class);
			if (index >= 0) {
				Throwable th = ExceptionUtils.getThrowableList(e).get(index);
				ConstraintViolationException cve = (ConstraintViolationException) th;
				String constraintName = cve.getConstraintName();
				final EgladilDuplicateEntryException egladilDuplicateEntryException = new EgladilDuplicateEntryException(
					th.getMessage(), e);
				egladilDuplicateEntryException.setUniqueIndexName(constraintName);
				return Optional.of(egladilDuplicateEntryException);
			}
		}
		return Optional.empty();
	}

	/**
	 * Fummelt aus der Exceptionkette die OptimisticLockException heraus, wenn sie drin ist und gibt sie in eine
	 * EgladilConcurrentModificationException verpackt zurück.
	 *
	 * @param Exception
	 * @return Optional von OptimisticLockException
	 */
	public static Optional<EgladilConcurrentModificationException> toEgladilConcurrentModificationException(Exception e) {
		if (e instanceof PersistenceException) {
			int index = ExceptionUtils.indexOfThrowable(e, OptimisticLockException.class);
			if (index >= 0) {
				Throwable th = ExceptionUtils.getThrowableList(e).get(index);
				return Optional.of(new EgladilConcurrentModificationException(th.getMessage(), e));
			}
		}
		return Optional.empty();
	}
}
